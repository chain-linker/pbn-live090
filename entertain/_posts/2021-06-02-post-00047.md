---
layout: post
title: "호국보훈의 달 추천영화"
toc: true
---

 6월은 [호국보훈의 달] 이라고 불리웁니다.
나라를 위해 희생하신 모든 순국선열과 호국영령을 추모하며
경건하고 엄숙하게 보내는 달이라고 합니다.
아직까지도 전쟁으로 인해 전장에서 포로가 되어
돌아오지 못하는 미송환 군 포로 문제를 생각해야 하며,
천안함 사건이나 연평해전 사건은 잊을 명맥 없는 우리 대한민국의
크나큰 슬픔이라 말할 행운 있습니다.
 6월 1일 의병의 날
 6월 6일 현충일
 6월 10일 민주항쟁 기념일
 6월 25일 한국전쟁
 6월 29일 연평해전

 오늘은 이러한 호국보훈의 달을 맞이하여
역사도 또 알아보고 재밌게 볼 행우 있는 영화들을 추천해볼까 합니다.

## 1. 아일라 Ayla
 포화 안 생에 새로 없을 운명 같은 만남,
그리고 60년간의 그리움
한국전쟁 참전병 슐레이만
5살소녀 아일라의 흉금 뜨거운 감동실화 이야기.
1950년 한국전쟁에 파병된 터키 군인 슐레이만은
칠흑같은 암흑 넋 독이 남겨진 5살 소녀를 발견하게 되고,
전쟁과 부모를 잃은 충격속에서 말을 잃어버린 소녀.
슐레이만은 소녀에게 아일라(터키어로 월광 이라는 뜻)라는 이름을 지어주고
함께 부대로 향하게 됩니다.
서로에게 무엇과도 바꿀 핵 없는 남편 소중한 존재가 되어버린 두사람.
하지만, 슐레이만은 종전과 함께 고국으로 돌아가라는 명령을 받게 되고,
아일라를 끝까지 지켜내기 위해 모두의 불찬 속에 자신의 선택을 감행하게 되는데...
네티즌 평점 9.3
 아일라 소녀역을 맡은 주인공은 김설.
응답하라1988에서 진주역으로 나와서 국민들에게 많은 사랑을 받았던 김설 양.
아일라역을 맡으며 짐짓 가슴아픈 소녀역을 즉금 표현해 준 것 같습니다.

## 2. 고지전
 1953년 2월 휴전협상이 난항을 거듭하게 되는 가운데
교착전이 한창인 동부전선 최전방 애록고지
전사한 중대장의 시신에서 아군의 총알이 발견됩니다.
상부에서는 이번 사건을 적과의 내통과 관련되어 있다고 의심하고
방첩대 중위 강은표에게 동부전선에서 조사하라는 임무를 맡깁니다.
애록고지에 중간 은표는 노형 [무료영화](https://leaktree.com/entertain/post-00004.html) 곳에서 죽은 줄 알았던 우인 김수혁을 만나게 되고,
유약한 학생이었던 수혁은 2년 사이에 이등병에서 중위로 특진해 악어중대의
실질적 리더가 되어 있엇으며 그가 함께하는 악어중대는 명성과 달리
춥다고 북한 군복을 덧입는 모습을 보이고 금세 스무 살이 된 어린 청년이 대위로 부대를 이끄는 등
수상한 일들이 벌어지고 있었습니다.
사라진 지난 2년,
그들에게 요컨대 무슨일이 있었던 것일까?

## 3. 연평해전

참고로 이문 영화는 한국인이라면 내처 봐야한다고 생각합니다.
그날의 아픔을 결단코 잊지말고, 그분들의 숭고한 희생을 첩경 기억해야 한다고 생각합니다.
연평해전, 천안함사건 정말로 대한민국 국민들에게는 더없이 가슴아픈 일입니다.
제2연평해전이 더더욱 가슴아픈건, 그때가 2002년 6월 29일
월드컵으로 온 국민이 하나가 되었다지만,
한편으로 해군들은 바다에서 나라를 지키기위해 숭고한 희생을 치르게 되었습니다.

영화 연평해전은 2002년 6월온국민이 월드컵의 함성으로 가득했었던 3,4위 한국과 터키의 경기가 있던 그날,대한민국을 지키기 위해 목숨을 걸고 싸웠던 사람들과 그들의 동료, 연인, 가족의 이야기를 그린 작품입니다.그날의 실화와 본체 인물을 영화적으로 재구성한 <연평해전>현실감과 진정성을 더해 감동 드라마로 탄생했다고 합니다.

 2002년6월29일 그날 잔야 10시경 서해 연평도에서 북한의 등산곶684호가대한민국 참수리 357호 고속정을 기습 공격해 해상 전투가 발발했으며, 기습 함포 공격을 시작으로 상호간치열한 격전이 약 30분간 진행되었다고 합니다. 30분간의 교화 끝에 대한민국은 20여명의 사상자가 발생하였고,참수리 357호 고속정이 침몰되었습니다.

 영화 연평해전은 단순한 영화 네놈 이상의 의미를 갖고있습니다."이 전투로 인해서 출혈 당한 사람들과 유가족분들에 대한 생각이 깊어졌다. 이익금 영화를 하면서자신이 태어난 나라에 대한 애정과 사랑, 관심을 우리가 몽땅 같이 소중하게 간직해야 한다는 것을 무지무지 느꼈다"라는 김학순 감독의 이야기처럼 잊혀져 가는 역사의 한 페이지를 되살려 많은 사람들에게 알릴 고갱이 있는뜻깊은 기회이기도 합니다.

 특별히 이러한 뜻에 동참한 국민들이 영화 연평해전의 제작을 위하여 큰힘을 모았으며,온 국민이 우리가 흡사 기억해야 할 그날의 실화에 공감하며 크라우드 펀딩을 통해 총 3차례에 거쳐 후원금을모아 제작을 도왔으며, 온 가족이 다름없이 모은 돼지저금통을 기부한 농부부터 아들을 군대에 보낸 가정주부,중고등학생까지, 세대와 계층을 초월해 진심을 보냈다고 합니다.
 호국보훈의 달을 맞이하여 3편의 영화를 포스팅 해보았습니다. 취중 종당 소개한 연평해전 영화는 6월을 맞이하여 어김없이 다들 보셨으면 좋겠습니다. 잊지않겠습니다 여부없이 기억하겠습니다.

#### 'Daily 잡지식' 카테고리의 다른 글
