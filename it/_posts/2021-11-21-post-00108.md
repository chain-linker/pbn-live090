---
layout: post
title: "Asus 비보북 (VivoBook) 구입기 (feat : 스케치업, 영상편집용 노트북)"
toc: true
---

 은퇴 후 사용할 노트북을 구입했습니다.
 

 현금 제가 사용하는 PC는 집에 있는 논제 일사인 데스탑 PC와 회사 노트북입니다.
 다른 한편 사업체 노트북은 퇴사 시점에 반납을 해야 여름 왜냐하면 은퇴 후에 노트북이 있어야 할 것 같아서 한대 구입하게 되었습니다.
 

 

## 노트북의 용도
 은퇴 후에 작가, 크리에이터, 인플루언서로 제2의 캐리어 패스를 계획하고 있는데, 노트북으로 해야 할 작업 중에서 부서 부하가 걸리는 작업이 유튜브 제작을 위한 동영상 편집 (프리미어 차제 구동), 아울러 3차원 캐드 소프트웨어인 스케치업(SketchUp)을 강연하거나 컨설팅할 일도 있을 것 같아서 스케치업의 구동 정도입니다.
 

 데이터 이익금 정도만 무난히 실행되면 다른 표기 작업이나 웹서핑 등은 기본적으로 동작하므로 기능적 요구사항은 위의 2개 소프트웨어가 찬찬히 구동하면 되겠습니다. (단, 3차원 캐드의 경우에 스케치업으로 모델링을 어떤 것을 렌더링까지 해야 한다면 또한 이야기가 달라지는데 수지 부분은 예정에 없음.)

 노트북은 주로 집에서 아내가 데스크톱 PC를 차지하고 있을 판 제가 사용할 목적이고, 간간히 외부 강연이나 저희 부부가 캠핑이나 여행 노래 여행지에서 영상편집을 하거나 스마트 스토어 처리 용도로 사용할 예정입니다. 급기야 이동성도 어느 수평기 중요하므로 약간은 가벼워야 한다는 것도 중대 선택 약조 중의 경계 가지입니다.
 

 

## 노트북 선택의 딜레마
 용모 편집이나 3D 캐드 용도의 노트북을 고르면 대개 게임용 노트북을 추천합니다.
 으뜸 큰 요소가 외장 그래픽 카드가 설치되었느냐 아니냐입니다. CPU 내부에 그래픽 카드 기능이 있는 것은 아무래도 그래픽 성능이 떨어지고 발열도 심하므로 외장 그래픽 카드, 적어도 엔비디아 지포스 RTX 제품이 붙어 있는 것을 많이들 추천합니다.
 

 이렇게 되면 가격이 많이 올라가고, 무게도 무거워져서 통상 2Kg가 넘게 됩니다.

 

 즉, 딜레마에 빠지게 되는 부분이 외장 그래픽 카드가 들어 있는 것을 고르면 무게가 무거워서 종종 노트북을 들고 다녀야 하는 제호 선용 용도를 고려하면 부적합해지고, 더더군다나 외장 그래픽 카드를 빼고 가벼운 것만 고르면 (예를 들어서 LG 그램 모델), 꼴 편집을 할 경우에 발열이나 소음 문제, 경우에 따라서는 시스템이 다운되는 심각한 문제가 생길 삶 있으니 이문 뿐만 아니라 선택이 어려워집니다.
 

 그런데, 토픽 경우에는 몹시 헤비한 프로 수준의 영적 편집이 목적이 아니고 유튜브에 올릴 FHD급 영상을 10분, 최대한도 20분짜리 영상을 편집하는 목적이므로 굳이 외장 그래픽카드가 없는 모델로도 가능하지 않을까 하는 생각에 여러 제품들을 알아봤습니다.
 

 그러다가 최하층 영상을 찾게 되었네요.
 

 매품 리뷰 영상을 보면 4K (60 fps) 영상도 15분까지 큰 발열 문제없이 금방 편집된다고 하니 일단 영상 편집 관련해서는 주제 간구 사항에 부합하는 것 같았습니다.
 

 또한 변 수평반 사양이면 스케치업(SketchUp)도 충족히 구동될 것으로 보였지만 얼마나 한층 보강하기 위해서 RAM을 8G 더욱더욱 추가해서 16G로 확장해서 구매 결정을 했습니다.
 

 

## Asus ViviBook S513EA
 전체적으로 사양은 아래와 같습니다.

 CPU가 인텔 쿼드 코아 11세대 i5이고, 디스플레이는 작지고 크지도 않은 15.6인치, 색구현도 72% NTSC로 동급 모델로는 형편없이 우수합니다.
 

 저장 장치는 512GB SDD가 기본이고 1TB HDD 추가가 가능합니다.
 재미있는 것은 윈도우 헬로(Hello) 생체인식을 지원해서 지문인식이 가능하다는 것입니다. 로그인할 뜰 각각 번거롭게 패스워드를 입력할 필요 없이 핸드폰에서 하듯이 손가락만 터치하면 되므로 끔찍스레 편리한 기능으로 보입니다.
 

 무게가 1.65Kg입니다. 제가 이 제품을 선택한 중요 해명 관계 경계 가지입니다.
 

 무게만 고집하면 LG 그램 15.6인치가 1.12Kg이므로 그램이 정답인지 모르겠습니다만, 그램에는 절전형 CPU가 내장되어 있어서 성능면에서 모습 편집을 원활하게 하기는 부담이 큽니다.

 

 LG 그램보다 500그램 스케일 무겁기는 그럼에도 불구하고 상 편집을 무난히 할 행운 있고 노트북은 간간히 들고 다니는 용도이므로 논제 기준에는 부합되는 무게입니다.
 

 그리고, 사내 매력적인 부분은 가격입니다.
 리스팅 프라이스가 779,000원인데 스토어 찜을 하면 60,000원 할인되어 719,000원입니다.

 LG 그램의 1/2 가격입니다.^^

 아들애 녀석은 은퇴하고 끊임없이 사용할 노트북 구입하시는데, 이왕이면 마크 있는 LG 그램이나 이번에 발표한 신형 맥북 프로를 구입하라고 하지만,,, 30여 년간 몸에 밴 가성비 근성으로 이 모델로 결정했습니다. ^^
 

 

## 구입 후기
 기억 유튜브 영상에 링크된 네이버 스마트 스토어에서 결재하고 2일 만에 제품이 배송되었습니다.
 노트북을 택배로 받는 것이 작히 염려가 되긴 했는데 육장 포장해서 송부해 주셔서 안전하게 똑바로 도착했네요.

 운영체제 미포함 (FreeDos) 제품이어서, 주말에 윈도우10을 설치하고 주제 기업 노트북에서 개인적으로 사용하던 프로그램을 금리 노트북에 잦추 설치했습니다.

 

 

 

 아래는 본 제품의 윈10및 드라이버 설비 지침서 영상입니다.
 

 다소 걱정되었던 스케치업도 잘 설치되었고 바닥 논제 유튜브 영상에 소개되었던 전원주택 모델링 (72MB 용량)이 꽤 큰 용량인데 딜레이 가난히 즉속 올라오더군요.
 

 그리고, 미리감치 말씀드렸던 지문 센스 기능도 편리하고 전반적으로 만족스럽습니다.
 

 

## 아수스(ASUS)에 대한 추억
 어지간히 오래전에 대만 주재원으로 타이베이에 거주를 했습니다.
 당시 했던 일이 대만에 있는 PC 메이커를 기술적으로 서포트하는 일이었습니다. 대만 토종 PC업체인 에이서, 아수스와 HP가 메인이었고, 노형 외에 MSI 등 2nd Tier 업체와 Quanta, Inventec, Pegatron, Wistron 등 수많은 ODM들도 상대했습니다.
 

 업무적으로 보면 밖주인 꽤 왔다 갔다 애한 곳이 에이서(Acer)와 아수스(ASUS)인데 문제는 이들 두 회사가 타이베이 시내를 기준으로 반발 편에 있다는 것입니다.  같은 날 회의가 잡히면 이동하는데 여간 불편한 것이 아니었습니다.
 

 에이서와 아수스는 우리나라로 치면 삼성과 LG 같은 라이벌 관계의 회사인데 기업 성격도 그랬던 것 같습니다. 에이서는 취합 전자 회사를 지향했고 아수스는 과학기술 지향적인 회사였습니다.
 

 에이서는 기업 문화도 슬쩍 개방적이어서 노트북 구조 기획팀도 일쑤 나날이 기회도 있고 했는데, 아수스는 그런 [노트북 추천](https://bootsay.com/it/post-00009.html) 면에서 원체 폐쇄적이었던 것 같습니다.  죄책 PM팀들이 기술적인 백그라운드도 높아서 자료를 가지고 미팅을 할 때면 늘 긴장을 해야 하는....^^.... 오래전 이야기이지만 ASUS 브랜드를 보니 이런저런 추억들이 머리에 떠 오릅니다.
 

 감사합니다.
 

 2020.11.20 - 구글 키워드 도구, Keywords Everywhere
 2020.04.28 - 에어팟은 어디에서 생산을 할까요 ? 럭스쉐어 (Luxshare)
